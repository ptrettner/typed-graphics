# Concepts

## Memory

* Allows easy use of aliased memory

## Pipelines

* Builder pattern to describe and verify a complete rendering pipeline
* Is built immediate mode and NOT declarative
* Can be trivially converted between init-time and render-time building

## RenderPass

* With sub passes
* Define structure of a rendering process

## CommandBuffer

* Records commands and submits them to a queue
* Corresponds to Vulkan CommandBuffers
* Threadlocal command pools in vulkan allow multithreaded recording


## Resource Reloading

Supported resources:

* Shaders
* Textures/Images
* Meshes
* ...

These resources can also be reloading in different threads.

Idea:

* Reloading is only valid inside a special code region
* Should not affect performance _at all_
* All objects are generational and thus easy to validate

## TODO

* sparse memory
* queries
* conditional rendering
* graph that displays all dependencies (execution, memory, fences, semaphores, ...)
* Multi view rendering
* Tiled rendering via vkGetRenderAreaGranularity
* Variable rate shading
* Mesh and task shading
* Subgroup computing
* Pipeline caches und derivative pipelines
* Specialization constants (e.g. for workgroup size)
* Automatically generate headers / declarations for shaders (using a `#pragma`)
* Transform feedback
