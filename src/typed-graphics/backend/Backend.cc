#include "Backend.hh"

#include <typed-geometry/common/assert.hh>

#ifdef TG_BACKEND_VULKAN

#include "vulkan/VulkanBackend.hh"

tg::SharedBackend tg::backend::vulkan(features features) { return std::make_shared<VulkanBackend>(features); }

#else
tg::SharedBackend tg::backend::vulkan(features features)
{
    TG_ASSERT(false, "Vulkan support not enabled (missing 'volk' dependency?)");
    return nullptr;
}
#endif


tg::SharedBackend tg::backend::recommended(features features)
{
#ifdef TG_BACKEND_VULKAN
    return vulkan(features);
#else
    TG_ASSERT(false, "No supported backend found");
    return nullptr;
#endif
}
