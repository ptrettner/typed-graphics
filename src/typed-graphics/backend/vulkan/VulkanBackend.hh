#pragma once

#if TG_BACKEND_VULKAN

#include <vector>

#include <typed-graphics/data/features.hh>

// vulkan loader
#include <volk.h>

// AFTER volk
#include <vulkan/vulkan.hpp>

#include "../Backend.hh"

namespace tg
{
namespace backend
{
/**
 * The vulkan backend represents a single vulkan instance
 *
 * Each device is a vulkan device
 */
class VulkanBackend : public Backend
{
    TG_REFERENCE_TYPE(VulkanBackend);

public:
    struct device_info
    {
        int priority;
        vk::PhysicalDevice device;
        std::string name;
    };

    struct device
    {
        vk::Device vk_device;
        vk::Queue vk_queue_graphics;
        vk::Queue vk_queue_compute;
    };

public:
    VulkanBackend(features features);
    ~VulkanBackend();

public:
    void submitGraphicsCommands(untyped::Device& device, untyped::CommandBuffer& cmdBuffer) override;

private:
    vk::Instance _instance;
    vk::DebugUtilsMessengerEXT _debugMessenger;
    std::vector<device_info> _supported_devices;
    // TODO: vk::Queue _queueTransfer;

	// "handle"d data
private:
	std::vector<device> _devices;
};
} // namespace backend
} // namespace tg
#endif
