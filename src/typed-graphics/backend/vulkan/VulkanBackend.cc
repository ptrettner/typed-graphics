#if TG_BACKEND_VULKAN

#include "VulkanBackend.hh"

#include <typed-graphics/common/exceptions.hh>
#include <typed-graphics/common/logging.hh>

#include <typed-graphics/untyped/CommandBuffer.hh>
#include <typed-graphics/untyped/Device.hh>

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                                                    VkDebugUtilsMessageTypeFlagsEXT messageType,
                                                    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
                                                    void* pUserData)
{
    switch (messageSeverity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        tg::diagnostic() << "validation layer: " << pCallbackData->pMessage;
        break;

    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
        tg::info() << "validation layer: " << pCallbackData->pMessage;
        break;

    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
        tg::warning() << "validation layer: " << pCallbackData->pMessage;
        break;

    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
        tg::error() << "validation layer: " << pCallbackData->pMessage;
        break;

    default:
        break;
    }

    return VK_FALSE;
}

namespace tg
{
namespace backend
{
VulkanBackend::VulkanBackend(features features)
{
    // initialize volk
    if (volkInitialize() != VK_SUCCESS)
    {
        error() << "Unable to initialize volk vulkan loader";
        throw failed_to_initialize_exception();
    }

    // enumerate extensions
    {
        for (auto const& ext : vk::enumerateInstanceExtensionProperties())
            diagnostic() << "EXTENSION: " << ext.extensionName << " v" << ext.specVersion;
        for (auto const& layer : vk::enumerateInstanceLayerProperties())
        {
            diagnostic() << "LAYER: " << layer.layerName << " v" << layer.specVersion << "/" << layer.implementationVersion << ": " << layer.description;

            // for (auto const& ext : vk::enumerateInstanceExtensionProperties(std::string(layer.layerName)))
            //     diagnostic() << "  " << ext.extensionName << " v" << ext.specVersion;
        }
    }

    // create vulkan instance
    {
        std::vector<char const*> layers;
        std::vector<char const*> extensions;

        if (features.debug)
        {
            layers.push_back("VK_LAYER_LUNARG_standard_validation");
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        // TODO: verify layer support

        vk::ApplicationInfo ai;
        ai.pApplicationName = "Typed Graphics Application";
        ai.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
        ai.pEngineName = "Typed Graphics";
        ai.engineVersion = VK_MAKE_VERSION(0, 1, 0);
        ai.apiVersion = VK_API_VERSION_1_1;

        vk::InstanceCreateInfo ii;
        ii.pApplicationInfo = &ai;
        ii.ppEnabledLayerNames = layers.data();
        ii.enabledLayerCount = i32(layers.size());
        ii.ppEnabledExtensionNames = extensions.data();
        ii.enabledExtensionCount = i32(extensions.size());

        _instance = createInstance(ii);

        volkLoadInstance(_instance);
    }

    // register debug callback
    if (features.debug)
    {
        vk::DebugUtilsMessengerCreateInfoEXT ci;
        ci.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | //
                             vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo |    //
                             vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | //
                             vk::DebugUtilsMessageSeverityFlagBitsEXT::eError;
        ci.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |      //
                         vk::DebugUtilsMessageTypeFlagBitsEXT ::ePerformance | //
                         vk::DebugUtilsMessageTypeFlagBitsEXT ::eValidation;
        ci.pfnUserCallback = debugCallback;
        ci.pUserData = nullptr; // Optional
        _debugMessenger = _instance.createDebugUtilsMessengerEXT(ci);
    }

    // devices
    for (auto const& dev : _instance.enumeratePhysicalDevices())
    {
        auto f = dev.getFeatures();
        auto p = dev.getProperties();

        diagnostic() << "DEVICE: " << p.deviceName << " v" << p.apiVersion << " id" << p.deviceID << " type " << int(p.deviceType);

        // feature check
        if (features.anisotropic_filtering && !f.samplerAnisotropy)
            continue;

        if (features.tessellation && !f.tessellationShader)
            continue;

        if (features.geometry_shader && !f.geometryShader)
            continue;

        // for now
        if (p.deviceType != vk::PhysicalDeviceType::eDiscreteGpu && p.deviceType != vk::PhysicalDeviceType::eIntegratedGpu)
            continue;

        // TODO: more

        auto prio = 1000;
        if (p.deviceType == vk::PhysicalDeviceType::eDiscreteGpu)
            prio += 1000;
        prio += p.limits.maxImageDimension2D;

        _supported_devices.push_back({prio, dev, p.deviceName});
    }
    sort(_supported_devices.begin(), _supported_devices.end(), [](auto const& a, auto const& b) { return a.priority > b.priority; });
    for (auto const& d : _supported_devices)
        diagnostic() << "SUITABLE DEVICE: (score " << d.priority << ") - " << d.name;
}

VulkanBackend::~VulkanBackend()
{
    if (_debugMessenger)
        _instance.destroy(_debugMessenger);

    _instance.destroy();
}

void VulkanBackend::submitGraphicsCommands(untyped::Device& device, untyped::CommandBuffer& cmdBuffer)
{
    auto const& d = _devices[device.handle().index()];

    vk::SubmitInfo si;
    si.commandBufferCount = 1;
    // TODO

    d.vk_queue_graphics.submit(si, {});

	// TODO: DEBUG for now
    d.vk_queue_graphics.waitIdle();
}
} // namespace backend
} // namespace tg

#endif