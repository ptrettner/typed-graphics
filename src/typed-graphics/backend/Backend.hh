#pragma once

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/data/features.hh>

#include <typed-graphics/untyped/untyped.hh>

namespace tg
{
namespace backend
{
/// returns the best recommended backend
SharedBackend recommended(features features = {});

SharedBackend vulkan(features features = {});
// SharedBackend glow();
// SharedBackend lava();
// SharedBackend opengl();
// SharedBackend software();
// SharedBackend remote();
} // namespace backend

class Backend
{
public:
    virtual ~Backend() {}

    // interface:
public:
    // TODO: array version
    virtual void submitGraphicsCommands(untyped::Device& device, untyped::CommandBuffer& cmdBuffer) = 0;
};
} // namespace tg
