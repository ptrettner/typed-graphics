#pragma once

namespace tg
{
enum class topology
{
    points,
    lines,
    triangles,

    line_strip,
    triangle_strip,
    triangle_fan,

    // TODO: adjacency?
};
}
