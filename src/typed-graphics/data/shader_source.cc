#include "shader_source.hh"

#include <typed-graphics/common/logging.hh>

#include <fstream>

tg::shader_source tg::shader_source::from_file(std::string const& filename)
{
    std::ifstream file(filename);
    if (!file)
    {
        error() << "Cannot open shader source file `" << filename << "'";
        return {};
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    return shader_source(buffer.str());
}
