#pragma once

namespace tg
{
enum class cull_mode
{
    none,
    cull_cw,
    cull_ccw,
    cull_all
};
}