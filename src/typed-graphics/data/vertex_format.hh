#pragma once

#include <vector>

#include <typed-graphics/common/macros.hh>
#include <typed-graphics/common/traits.hh>

namespace tg
{
/**
 * Format of a single vertex
 *
 * A vertex shader can be fed from of multiple vertex streams
 */
struct vertex_format
{
    struct attribute
    {
        int offset;
        type type;
        std::string name;

        attribute() = default;
        template <class A, class B>
        attribute(A B::*attr, std::string name)
          : offset(static_cast<int>(reinterpret_cast<uint64_t>(&(static_cast<B*>(nullptr)->*attr)))), //
            type(type_of<A>),
            name(std::move(name))
        {
        }
    };

    int stride;
    std::vector<attribute> attributes;
};
} // namespace tg
