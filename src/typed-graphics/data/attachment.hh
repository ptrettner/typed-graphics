#pragma once

#include <typed-graphics/tg-lean.hh>

namespace tg
{
template <class T>
struct attachment
{
    std::string name;
    // TODO: image

    attachment(SharedImage2D<T> const&, std::string name) : name(std::move(name)) {}
};

template <class T>
attachment<T> make_attachment(SharedImage2D<T> const& img, std::string name)
{
    return {img, std::move(name)};
}
} // namespace tg
