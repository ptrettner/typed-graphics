#pragma once

#include <string>

namespace tg
{
/**
 * Opaque value type for holding the source code of a shader
 *
 * Usage:
 *		auto shader = device->createVertexShader<vertex>(shader_source("void main() { gl_Position = ...; }"));
 *		auto shader = device->createFragmentShader<tg::f16vec3>(shader_source::from_file("/path/to/file.fsh"));
 */
struct shader_source
{
public:
    shader_source() = default;
    explicit shader_source(std::string source) : source(std::move(source)) {}

public:
    static shader_source from_file(std::string const& filename);

public:
    bool is_valid() const { return !source.empty(); }

private:
    std::string source;
};
} // namespace tg