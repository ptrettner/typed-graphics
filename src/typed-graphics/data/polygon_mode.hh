#pragma once

namespace tg
{
enum class polygon_mode
{
    fill,
    line,
    point
};
}