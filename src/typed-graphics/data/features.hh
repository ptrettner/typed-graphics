#pragma once

namespace tg
{
// feature::defaults() is about OGL 4.0
// feature::minimal() is about OGL 3.0
// feature::all() is bleeding edge everything (e.g. raytracing, sparse binding, ...)
struct features
{
    // "big" features
    bool debug = true;
    bool raytracing = false;
    bool tessellation = true;
    bool geometry_shader = true;
    bool transform_feedback = true;
    // TODO: more

    // "small" features
    bool anisotropic_filtering = true;
    // TODO: more

    static features defaults() { return {}; }
    static features minimal()
    {
        features f;
        f.tessellation = false;
        f.anisotropic_filtering = false;
        f.geometry_shader = false;
        f.transform_feedback = false;
        return f;
    }
    static features all()
    {
        features f;
        f.raytracing = true;
        return f;
    }
};
} // namespace tg