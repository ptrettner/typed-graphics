#pragma once

namespace tg
{
// "shape" of a type
enum class type
{
    i32,
    u32,
    f32,

    f8vec1,
    f8vec2,
    f8vec3,
    f8vec4,

    f16vec1,
    f16vec2,
    f16vec3,
    f16vec4,

    f32vec1,
    f32vec2,
    f32vec3,
    f32vec4,

	invalid
};
} // namespace tg