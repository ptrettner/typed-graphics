#pragma once

#include <cstring>
#include <memory>
#include <vector>

#include <typed-geometry/types/scalar.hh>

#include "handle.hh"
#include "untyped.hh"

namespace tg
{
namespace untyped
{
enum class command : u8
{
    // passes
    begin_render_pass,
    end_render_pass,

    // bindings
    bind_primitive_pipeline,
    bind_vertex_buffers,
    bind_index_buffer,

    // draw
    draw,
    draw_indexed,

    // compute
    // TODO

    // raytracing
    // TODO
};

namespace detail
{
struct command_vector_allocator
{
    std::vector<char> data;

    char* alloc(u64 size)
    {
        auto s = data.size();
        data.resize(s + size);
        return data.data() + s;
    }
};
} // namespace detail

/**
 * Container for recording commands
 */
template <class CommandAllocator = detail::command_vector_allocator>
struct command_writer
{
    command_writer(CommandAllocator pool = {}) : pool(std::move(pool)) {}

    // passes
public:
    void beginRenderPass(handle<RenderPass> h)
    {
        write(command::begin_render_pass);
        write(h);
    }
    void endRenderPass(handle<RenderPass> h)
    {
        write(command::end_render_pass);
        write(h);
    }

    // bindings
public:
    void bindPrimitivePipeline(handle<PrimitivePipeline> h)
    {
        write(command::bind_primitive_pipeline);
        write(h);
    }
    // TODO: multi buffer version
    void bindVertexBuffers(handle<BufferView> buffer, u64 offset)
    {
        write(command::bind_vertex_buffers);
        write(1u);
        write(buffer);
        write(offset);
    }
    void bindIndexBuffer(handle<BufferView> buffer, u64 offset, u32 index_bits)
    {
        write(command::bind_index_buffer);
        write(buffer);
        write(offset);
        write(index_bits);
    }

    // drawing
public:
    void draw(u32 vertex_count, u32 instance_count, u32 first_vertex, u32 first_instance)
    {
        write(command::draw);
        write(vertex_count);
        write(instance_count);
        write(first_vertex);
        write(first_instance);
    }

private:
    CommandAllocator pool;

    template <class T>
    void write(handle<T> h)
    {
        write_raw(h);
    }
    void write(command c) { write_raw(c); }
    void write(u32 v) { write_raw(v); }
    void write(i32 v) { write_raw(v); }
    void write(u64 v) { write_raw(v); }
    void write(i64 v) { write_raw(v); }
    void write(f32 v) { write_raw(v); }

    template <class T>
    void write_raw(T const& v)
    {
        auto d = pool.alloc(sizeof(T));
        memcpy(d, &v, sizeof(T));
    }
};
} // namespace untyped
} // namespace tg
