#pragma once

#include <vector>

#include "UntypedObject.hh"
#include "untyped.hh"

namespace tg
{
namespace untyped
{
class CommandBuffer : public UntypedObject<Device>
{
};
} // namespace untyped
} // namespace tg