#pragma once

#include <typed-geometry/types/scalar.hh>

namespace tg
{
namespace untyped
{
template <class T>
struct handle
{
    constexpr bool is_valid() const { return _generation > 0; }

    constexpr u32 generation() const { return _generation; }
    constexpr u32 index() const { return _index; }

    constexpr handle() = default;
    constexpr handle(u32 index, u32 gen) : _index(index), _generation(gen) {}

private:
    u32 _index = 0;
    u32 _generation = 0;

public:
    constexpr bool operator==(handle<T> const& rhs) const { return _index == rhs._index && _generation == rhs._generation; }
    constexpr bool operator!=(handle<T> const& rhs) const { return _index != rhs._index || _generation != rhs._generation; }

    constexpr operator bool() const { return is_valid(); }
};
} // namespace untyped
} // namespace tg