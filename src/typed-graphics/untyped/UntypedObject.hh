#pragma once

#include <memory>

#include <typed-graphics/common/macros.hh>

#include "handle.hh"

namespace tg
{
namespace untyped
{
template <class ThisT>
class UntypedObject
{
    TG_REFERENCE_TYPE(UntypedObject);

public:
    using this_t = ThisT;

    using data_deleter = void(void*);

    UntypedObject() = default;

public:
    int generation() const { return _generation; }
    void invalidate() { _generation++; }

	// if handle.generation() != generation(), the backend will regenerate the data
    untyped::handle<ThisT> handle() const { return _handle; }

private:
    // NOTE: generation 0 is invalid!
    int _generation = 1000; // high start to catch invalid memory

    untyped::handle<ThisT> _handle;
};
} // namespace untyped
} // namespace tg