#pragma once

#include "UntypedObject.hh"

namespace tg
{
namespace untyped
{
class BufferView : public UntypedObject<BufferView>
{
};
} // namespace untyped
} // namespace tg