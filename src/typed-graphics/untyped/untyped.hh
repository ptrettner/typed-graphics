#pragma once

namespace tg
{
namespace untyped
{
class Buffer;
class BufferView;
class Image;
class ImageView;
class CommandBuffer;
class Device;
class Framebuffer;
class Window;
class RenderPass;

class PrimitivePipeline;
} // namespace untyped
} // namespace tg
