#pragma once

#include "UntypedObject.hh"

#include <typed-graphics/tg-lean.hh>

namespace tg
{
namespace untyped
{
class Device : public UntypedObject<Device>
{
public:
    SharedBackend const backend;

public:
    Device(SharedBackend backend) : backend(std::move(backend)) {}
};
} // namespace untyped
} // namespace tg