#pragma once

#include <memory>

#include <typed-geometry/common/assert.hh>
#include <typed-graphics/common/macros.hh>
#include <typed-graphics/common/traits.hh>

// TODO: maybe better management?
namespace tg
{
TG_SHARED(class, Backend);
TG_SHARED(class, Device);

TG_SHARED(class, Window);

TG_SHARED(class, GraphicsCommands);
TG_SHARED(class, ComputeCommands);

TG_SHARED(class, RenderPass);
TG_SHARED_T1(class, Framebuffer, FragmentT);

// pipelines
template <class VertexT, class FragmentT, class ConstantT = void, class... ResourceSets>
class PrimitivePipeline;
template <class VertexT, class FragmentT, class ConstantT = void, class... ResourceSets>
using SharedPrimitivePipeline = std::shared_ptr<PrimitivePipeline<VertexT, FragmentT, ConstantT, ResourceSets...>>;

TG_SHARED_T1(class, Buffer, DataT);
TG_SHARED_T1(class, BufferView, DataT);

TG_SHARED_T1(class, VertexShader, VertexT);
TG_SHARED(class, TessellationControlShader);
TG_SHARED(class, TessellationEvaluationShader);
TG_SHARED(class, GeometryShader);
TG_SHARED_T1(class, FragmentShader, FragmentT);
TG_SHARED(class, MeshShader);
TG_SHARED(class, TaskShader);
TG_SHARED(class, RayGenerationShader);
TG_SHARED(class, RayClosestHitShader);
TG_SHARED(class, RayAnyHitShader);
TG_SHARED(class, RayIntersectionShader);
TG_SHARED(class, RayMissShader);

template <class VertexT, class FragmentT>
class Shader;
template <class VertexT, class FragmentT>
using SharedShader = std::shared_ptr<Shader<VertexT, FragmentT>>;

template <int D, class DataT>
class Image;
template <class DataT>
using Image1D = Image<1, DataT>;
template <class DataT>
using Image2D = Image<2, DataT>;
template <class DataT>
using Image3D = Image<3, DataT>;
template <int D, class DataT>
using SharedImage = std::shared_ptr<Image<D, DataT>>;
template <class DataT>
using SharedImage1D = std::shared_ptr<Image1D<DataT>>;
template <class DataT>
using SharedImage2D = std::shared_ptr<Image2D<DataT>>;
template <class DataT>
using SharedImage3D = std::shared_ptr<Image3D<DataT>>;

template <int D, class DataT>
class ImageView;
template <class DataT>
using ImageView1D = ImageView<1, DataT>;
template <class DataT>
using ImageView2D = ImageView<2, DataT>;
template <class DataT>
using ImageView3D = ImageView<3, DataT>;
template <int D, class DataT>
using SharedImageView = std::shared_ptr<ImageView<D, DataT>>;
template <class DataT>
using SharedImageView1D = std::shared_ptr<ImageView1D<DataT>>;
template <class DataT>
using SharedImageView2D = std::shared_ptr<ImageView2D<DataT>>;
template <class DataT>
using SharedImageView3D = std::shared_ptr<ImageView3D<DataT>>;
} // namespace tg
