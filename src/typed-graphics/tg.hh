#pragma once

// Math library (only type definitions)
#include <typed-geometry/tg-lean.hh>

// Common
#include "common/logging.hh"
#include "common/macros.hh"
#include "common/trace.hh"
#include "common/traits.hh"

// Backend
#include "backend/Backend.hh"

// Data
#include "data/attachment.hh"
#include "data/cull_mode.hh"
#include "data/polygon_mode.hh"
#include "data/shader_source.hh"
#include "data/topology.hh"
#include "data/type.hh"
#include "data/vertex_format.hh"

// Builder
#include "builder/builder.hh"

// Objects
#include "objects/objects.hh"

// Shader
#include "shader/context.hh"
#include "shader/types.hh"
