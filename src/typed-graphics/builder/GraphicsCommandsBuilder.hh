#pragma once

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/untyped/commands.hh>

namespace tg
{
template <class CommandAllocator>
struct GraphicsCommandsBuilder
{
    TG_RAII_TYPE(GraphicsCommandsBuilder);

    // sub-builder
public:
    template <class VertexT, class FragmentT, class ConstantT, class... ResourceSets>
    struct PrimitivePipelineRecorder
    {
        TG_RAII_TYPE(PrimitivePipelineRecorder);

        PrimitivePipelineRecorder(untyped::command_writer<CommandAllocator>& cmd) : _cmd(cmd) {}

        // TODO: record current vb/ib for rebinding

        void bindResources()
        {
            // TODO
        }

        void draw(SharedBuffer<VertexT> const& vertexBuffer) { draw(vertexBuffer->view()); }
        void draw(SharedBufferView<VertexT> const& vertexBuffer)
        {
            // TODO: optimize bindings
            _cmd.bindVertexBuffers(vertexBuffer->handle(), 0);
            _cmd.draw(vertexBuffer->elementCount(), 1, 0, 0);
        }

        template <class PushConstants = ConstantT, class = std::enable_if_t<!std::is_same<PushConstants, void>::value>>
        void draw(PushConstants const& constants, SharedBuffer<VertexT> const& vertexBuffer)
        {
            draw(constants, vertexBuffer->view());
        }
        template <class PushConstants = ConstantT, class = std::enable_if_t<!std::is_same<PushConstants, void>::value>>
        void draw(PushConstants const& constants, SharedBufferView<VertexT> const& vertexBuffer)
        {
            (void)constants;
            // TODO: push constants
            _cmd.bindVertexBuffers(vertexBuffer->handle(), 0);
            _cmd.draw(vertexBuffer->elementCount(), 1, 0, 0);
        }

    private:
        untyped::command_writer<CommandAllocator>& _cmd;
    };
    struct RenderPassRecorder
    {
        TG_RAII_TYPE(RenderPassRecorder);

        RenderPassRecorder(untyped::command_writer<CommandAllocator>& cmd) : _cmd(cmd) {}

        template <class VertexT, class FragmentT, class ConstantT, class... ResourceSets>
        PrimitivePipelineRecorder<VertexT, FragmentT, ConstantT, ResourceSets...> recordPipeline(
            SharedPrimitivePipeline<VertexT, FragmentT, ConstantT, ResourceSets...> const& pipeline)
        {
            (void)pipeline;
            return {_cmd};
        }

    private:
        untyped::command_writer<CommandAllocator>& _cmd;
    };

    GraphicsCommandsBuilder(Device* autoSubmitToDevice = nullptr, CommandAllocator pool = {})
      : _autoSubmitToDevice(autoSubmitToDevice), _cmd(std::move(pool))
    {
    }

    // NOTE: this one is implemented in Device.hh
    ~GraphicsCommandsBuilder();

    // TODO

    RenderPassRecorder recordPass(SharedRenderPass const& pass)
    {
        (void)pass;
        return {_cmd};
    }
    RenderPassRecorder recordPass(SharedRenderPass const& pass, iaabb2 viewport)
    {
        (void)pass;
        (void)viewport;
        return {_cmd};
    }
    RenderPassRecorder recordPass(SharedRenderPass const& pass, iaabb2 viewport, iaabb2 scissor)
    {
        (void)pass;
        (void)viewport;
        (void)scissor;
        return {_cmd};
    }

private:
    Device* _autoSubmitToDevice;
    untyped::command_writer<CommandAllocator> _cmd;
};
} // namespace tg
