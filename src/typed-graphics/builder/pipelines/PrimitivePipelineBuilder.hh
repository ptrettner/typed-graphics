#pragma once

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/data/cull_mode.hh>
#include <typed-graphics/data/polygon_mode.hh>
#include <typed-graphics/data/topology.hh>

namespace tg
{
template <class VertexT, class FragmentT, class ConstantT = void, class... ResourceSets>
struct PrimitivePipelineBuilder
{
    PrimitivePipelineBuilder(SharedVertexShader<VertexT> const& vs, SharedFragmentShader<FragmentT> const& fs, topology topology = topology::triangles)
    {
        (void)vs;
        (void)fs;
        (void)topology;
        // TODO
    }

    PrimitivePipelineBuilder& tessellation(int patchSize, SharedTessellationControlShader const& tecShader, SharedTessellationEvaluationShader const& tevShader)
    {
        (void)patchSize;
        (void)tecShader;
        (void)tevShader;
        // TODO
        return *this;
    }
    PrimitivePipelineBuilder& geometryShader(SharedGeometryShader const& shader)
    {
        (void)shader;
        // TODO
        return *this;
    }

    PrimitivePipelineBuilder& cullMode(cull_mode cm)
    {
        cull_mode = cm;
        return *this;
    }
    PrimitivePipelineBuilder& polygonMode(polygon_mode pm)
    {
        polygon_mode = pm;
        return *this;
    }

private:
    cull_mode cull_mode = cull_mode::none;
    polygon_mode polygon_mode = polygon_mode::fill;

    // TODO:
    // - depth bias
    // - depth clamp
    // - line width
    // - rasterizer discard? - maybe own pipeline

    // TODO: color blend (https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPipelineColorBlendStateCreateInfo.html)
    // TODO: depth state (https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPipelineDepthStencilStateCreateInfo.html)
};
} // namespace tg
