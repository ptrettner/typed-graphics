#pragma once

#include <string>
#include <vector>

#include <typed-geometry/detail/scalar_traits.hh>

#include <typed-graphics/data/attachment.hh>
#include <typed-graphics/objects/RenderPass.hh>
#include <typed-graphics/tg-lean.hh>

#include "pipelines/PrimitivePipelineBuilder.hh"

namespace tg
{
struct RenderPassBuilder;
struct SubPassBuilder;

struct SubPassBuilder
{
    template <class T>
    void write(std::string name)
    {
        attachments.emplace_back(attachment_action::write, type_of<T>, std::move(name));
    }
    template <class T>
    void write(attachment<T> const& attachment)
    {
        // TODO: store attachment
        attachments.emplace_back(attachment_action::write, type_of<T>, attachment.name);
    }
    template <class T>
    void read(std::string name)
    {
        attachments.emplace_back(attachment_action::read, type_of<T>, std::move(name));
    }
    template <class T>
    void read(attachment<T> const& attachment)
    {
        // TODO: store attachment
        attachments.emplace_back(attachment_action::read, type_of<T>, attachment.name);
    }

    template <class ConstantT = void, class VertexT, class FragmentT>
    PrimitivePipelineBuilder<VertexT, FragmentT, ConstantT> buildPipeline(SharedVertexShader<VertexT> const& vs,
                                                                          SharedFragmentShader<FragmentT> const& fs,
                                                                          topology topo = topology::triangles)
    {
        return {vs, fs, topo};
    }

private:
    enum class attachment_action
    {
        write,
        read
    };

    // closely mirrors https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkAttachmentDescription.html
    struct rp_attachment
    {
        attachment_action action;
        type format;
        std::string name;

        rp_attachment() = default;
        rp_attachment(attachment_action a, type f, std::string n) : action(a), format(f), name(std::move(n)) {}
    };
    std::vector<rp_attachment> attachments;

    friend class Device;
};

struct RenderPassBuilder
{
    template <class T>
    void clearColor(std::string name, T const& clearValue)
    {
        clearInfos.push_back(clear_info(std::move(name), clearValue));
    }
    template <class T>
    void clear(attachment<T> const& attachment, T const& clearValue)
    {
        // TODO: store attachment instead
        clearInfos.push_back(clear_info(attachment.name, clearValue));
    }

    void clearDepth(std::string name, f32 clearDepth, u32 clearStencil = 0)
    {
        clear_info ci;
        ci.name = std::move(name);
        ci.clearDepth = clearDepth;
        ci.clearStencil = clearStencil;
        clearInfos.push_back(ci);
    }

    SubPassBuilder buildSubPass() { return {}; }

private:
    struct clear_info
    {
        type format = type::invalid;
        std::string name;

        vec4 clearColorF;
        ivec4 clearColorI;
        uvec4 clearColorU;
        float clearDepth = 0;
        u32 clearStencil = 0;

        // TODO: colors
        // TODO: proper upcast to vec4
        clear_info() = default;

        template <int D, class T, std::enable_if_t<is_floating_point<T>, int> = 0>
        clear_info(std::string name, vec<D, T> const& clearValue) : format(type_of<vec<D, T>>), name(std::move(name)), clearColorF(clearValue)
        {
        }
        template <int D, class T, std::enable_if_t<is_signed_integer<T>, int> = 0>
        clear_info(std::string name, vec<D, T> const& clearValue) : format(type_of<vec<D, T>>), name(std::move(name)), clearColorI(clearValue)
        {
        }
        template <int D, class T, std::enable_if_t<is_unsigned_integer<T>, int> = 0>
        clear_info(std::string name, vec<D, T> const& clearValue) : format(type_of<vec<D, T>>), name(std::move(name)), clearColorU(clearValue)
        {
        }
    };

    std::vector<clear_info> clearInfos;

    friend class Device;
};
} // namespace tg
