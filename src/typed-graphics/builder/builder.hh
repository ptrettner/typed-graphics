#pragma once

/**
 * Builder pattern for more complex objects
 * 
 * Use device->createXyz(XyzBuilder) to create object
 */

#include "RenderPassBuilder.hh"

#include "GraphicsCommandsBuilder.hh"

#include "pipelines/PrimitivePipelineBuilder.hh"
