#pragma once

#include "types.hh"

namespace tg
{
namespace shader
{
struct context
{
    /// in NDC
    vec4 position;
};
} // namespace shader
} // namespace tg
