#pragma once

namespace tg
{
template <class T>
struct shader_type_t
{
};
template <class T>
using shader_type_of = typename shader_type_t<T>::type;

namespace shader
{
// TODO: properly


struct vec2;
struct vec3;
struct vec4;
struct f8vec3;

struct vec2
{
};
struct vec3
{
    vec3() = default;
    vec3(f8vec3 const&) {}
    vec3(f16vec3 const&) {}
};
struct vec4
{
    vec4() = default;
    vec4(vec3 const&, float) {}
};

struct f8vec3
{
    f8vec3(vec3 const&) {}
};
struct f16vec3
{
    f16vec3(vec3 const&) {}
};
} // namespace shader

#define TG_DEFINE_SHADER_TYPE(FromT, ToT) \
    template <>                           \
    struct shader_type_t<FromT>           \
    {                                     \
        using type = ToT;                 \
    } // enforce ;

TG_DEFINE_SHADER_TYPE(vec2, shader::vec2);
TG_DEFINE_SHADER_TYPE(vec3, shader::vec3);
TG_DEFINE_SHADER_TYPE(vec4, shader::vec4);

TG_DEFINE_SHADER_TYPE(f8vec3, shader::f8vec3);
TG_DEFINE_SHADER_TYPE(f16vec3, shader::f16vec3);

} // namespace tg