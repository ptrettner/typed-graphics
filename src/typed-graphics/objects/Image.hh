#pragma once

#include <typed-geometry/tg-lean.hh>

namespace tg
{
template <int D, class DataT>
class Image : public std::enable_shared_from_this<Image<D, DataT>>
{
    TG_REFERENCE_TYPE(Image);

public:
    Image(size<D, int> size) : _size(size) {}

public:
    SharedImageView<D, DataT> view() const
    {
        // TODO: create default view
        return nullptr;
    }

    size<D, int> size() const { return _size; }

public:
    /// resizes this image
    /// NOTE: this is an expensive operation that rebuilds many resources
    ///       (should be used seldom, e.g. when window is resized)
    /// NOTE: only resizes if size is different (s == size() is a guaranteed no-op)
    void resize(tg::size<D, int> s)
    {
        if (s == _size)
            return;

        // TODO
        _size = s;
    }

public:
    using data_t = DataT;

    Image() = default;

private:
    tg::size<D, int> _size;
};
} // namespace tg
