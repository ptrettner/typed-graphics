#pragma once

#include <functional>

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/shader/context.hh>

namespace tg
{
template <class VertexT, class FragmentT>
class Shader
{
    TG_REFERENCE_TYPE(Shader);

public:
    using vertex_t = VertexT;
    using fragment_t = FragmentT;

    Shader() = default;
};
} // namespace tg
