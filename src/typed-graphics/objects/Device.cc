#include "Device.hh"

#include "objects.hh"

#include <typed-graphics/backend/Backend.hh>

namespace tg
{
SharedWindow Device::createWindow(isize2 size)
{
    // TODO
    (void)size;
    return std::make_shared<Window>();
}

SharedRenderPass Device::createRenderPass(RenderPassBuilder const& builder)
{
    // TODO
    (void)builder;
    return std::make_shared<RenderPass>();
}

SharedRenderPass Device::createRenderPass(SubPassBuilder const& builder)
{
    // TODO
    (void)builder;
    return std::make_shared<RenderPass>();
}

Device::Device(SharedBackend backend, use_create_method) : _object(std::move(backend)) {}

SharedDevice Device::create(SharedBackend backend)
{
    if (!backend)
        backend = backend::recommended();

    return std::make_shared<Device>(std::move(backend), use_create_method{});
}
} // namespace tg
