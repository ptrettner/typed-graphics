#pragma once

#include <functional>
#include <vector>

#include <typed-geometry/tg-lean.hh>
#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/shader/context.hh>

#include <typed-graphics/builder/builder.hh>

#include <typed-graphics/backend/Backend.hh>

#include <typed-graphics/data/features.hh>
#include <typed-graphics/data/shader_source.hh>

#include "pipelines/PrimitivePipeline.hh"

#include "shaders/FragmentShader.hh"
#include "shaders/VertexShader.hh"

#include "GraphicsCommands.hh"

#include <typed-graphics/untyped/Device.hh>
#include <typed-graphics/untyped/commands.hh>

namespace tg
{
class Device : public std::enable_shared_from_this<Device>
{
    TG_REFERENCE_TYPE(Device);

    // windows
public:
    SharedWindow createWindow(isize2 size);

    // buffers
public:
    template <class DataT>
    SharedBuffer<DataT> createBuffer(std::vector<DataT> const& data)
    {
        auto buffer = std::make_shared<Buffer<DataT>>();
        // TODO: upload
        (void)data;
        return buffer;
    }

    // images
public:
    template <int D, class DataT>
    SharedImage<D, DataT> createImage()
    {
        auto img = std::make_shared<Image<D, DataT>>();
        // TODO
        return img;
    }

    template <class DataT>
    SharedImage<1, DataT> createImage1D()
    {
        return createImage<1, DataT>();
    }
    template <class DataT>
    SharedImage<2, DataT> createImage2D()
    {
        return createImage<2, DataT>();
    }
    template <class DataT>
    SharedImage<3, DataT> createImage3D()
    {
        return createImage<3, DataT>();
    }

    // shaders
public:
    template <class VertexT, class FragmentT, class FunT>
    SharedShader<VertexT, FragmentT> createShader(FunT&& fun)
    {
        auto shader = std::make_shared<Shader<VertexT, FragmentT>>();
        // std::function<FragmentT(shader::context & ctx, VertexT v)> f = fun;
        // TODO: create shader from function
        // TODO: properly use shader types
        (void)fun;
        return shader;
    }

    template <class VertexT>
    SharedVertexShader<VertexT> createVertexShader(shader_source const& src)
    {
        (void)src;
        auto shader = std::make_shared<VertexShader<VertexT>>();
        // TODO
        return shader;
    }
    template <class FragmentT>
    SharedFragmentShader<FragmentT> createFragmentShader(shader_source const& src)
    {
        (void)src;
        auto shader = std::make_shared<FragmentShader<FragmentT>>();
        // TODO
        return shader;
    }

    // pipelines
public:
    SharedRenderPass createRenderPass(RenderPassBuilder const& builder);
    SharedRenderPass createRenderPass(SubPassBuilder const& builder);

    template <class VertexT, class FragmentT, class ConstantT, class... ResourceSets>
    SharedPrimitivePipeline<VertexT, FragmentT, ConstantT, ResourceSets...> createPrimitivePipeline(
        PrimitivePipelineBuilder<VertexT, FragmentT, ConstantT, ResourceSets...> const& builder);

    // commands
public:
    template <class CommandAllocator>
    SharedGraphicsCommands createCommandBuffer(GraphicsCommandsBuilder<CommandAllocator> const& builder)
    {
        auto cmds = std::make_shared<GraphicsCommands>();
        cmds->build(builder);
        return cmds;
    }

    template <class CommandAllocator = untyped::detail::command_vector_allocator>
    TG_NO_DISCARD GraphicsCommandsBuilder<CommandAllocator> submitGraphicsCommands()
    {
        return {this};
    }

    void submitGraphicsCommands(SharedGraphicsCommands const& cmdBuffer)
    {
        // should be inlined for high-performance
        _object.backend->submitGraphicsCommands(_object, cmdBuffer->_object);
    }

public:
    static SharedDevice create(SharedBackend backend = nullptr);

public:
    struct use_create_method
    {
    };
    Device(SharedBackend backend, use_create_method);

private:
    untyped::Device _object;
};

// ========= IMPLEMENTATION ==========

template <class VertexT, class FragmentT, class ConstantT, class... ResourceSets>
SharedPrimitivePipeline<VertexT, FragmentT, ConstantT, ResourceSets...> Device::createPrimitivePipeline(
    PrimitivePipelineBuilder<VertexT, FragmentT, ConstantT, ResourceSets...> const& builder)
{
    (void)builder;
    // TODO
    return std::make_shared<PrimitivePipeline<VertexT, FragmentT, ConstantT, ResourceSets...>>();
}

template <class CommandAllocator>
GraphicsCommandsBuilder<CommandAllocator>::~GraphicsCommandsBuilder()
{
    if (_autoSubmitToDevice)
        _autoSubmitToDevice->submitGraphicsCommands(_autoSubmitToDevice->createCommandBuffer(*this));
}

} // namespace tg
