#pragma once

#include <typed-geometry/tg-lean.hh>
#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/untyped/CommandBuffer.hh>

#include "Buffer.hh"
#include "BufferView.hh"
#include "Image.hh"
#include "ImageView.hh"
#include "RenderPass.hh"

#include "pipelines/PrimitivePipeline.hh"

#include <typed-graphics/builder/GraphicsCommandsBuilder.hh>

namespace tg
{
/**
 * TODO:
 *	* resettable command buffers
 *	* threadlocal command pools
 *	* transient command buffer (one-time)
 *
 * NOTE:
 *	any referenced object MUST outlive the command buffer
 */
class GraphicsCommands
{
    TG_REFERENCE_TYPE(GraphicsCommands);
    friend class Device;

public:
    GraphicsCommands() = default;

public:
    /// builds a command buffer from a given commands
    template <class CommandAllocator>
    void build(GraphicsCommandsBuilder<CommandAllocator> const& builder)
    {
        (void)builder;
        // TODO
    }

private:
    untyped::CommandBuffer _object;
};
} // namespace tg
