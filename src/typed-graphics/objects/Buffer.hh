#pragma once

#include <vector>

#include <typed-graphics/tg-lean.hh>

namespace tg
{
template <class DataT>
class Buffer : public std::enable_shared_from_this<Buffer<DataT>>
{
    TG_REFERENCE_TYPE(Buffer);

public:
    SharedBufferView<DataT> view() const
    {
        // TODO: create default view
        return nullptr;
    }

    u32 elementCount() const { return 0; /* TODO */ }

public:
    using data_t = DataT;

    Buffer() = default;
};

} // namespace tg
