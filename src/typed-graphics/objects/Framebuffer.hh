#pragma once

#include <typed-geometry/tg-lean.hh>

namespace tg
{
/**
 * A framebuffer specifies which images are sources/targets in a RenderPass
 */
template <class FragmentT>
class Framebuffer
{
    TG_REFERENCE_TYPE(Framebuffer);

public:
    using fragment_t = FragmentT;

	Framebuffer() = default;
};

} // namespace tg
