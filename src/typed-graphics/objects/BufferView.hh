#pragma once

#include <vector>

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/untyped/BufferView.hh>

namespace tg
{
template <class DataT>
class BufferView
{
    TG_REFERENCE_TYPE(BufferView);

public:
    u32 elementCount() const { return 0; /* TODO */ }

	untyped::handle<untyped::BufferView> handle() const { return _object.handle(); }

public:
    using data_t = DataT;

    BufferView() = default;

private:
    untyped::BufferView _object;
};

} // namespace tg
