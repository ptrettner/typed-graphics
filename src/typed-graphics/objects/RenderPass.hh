#pragma once

#include <typed-graphics/tg-lean.hh>

namespace tg
{
class RenderPass
{
    TG_REFERENCE_TYPE(RenderPass);

public:
    RenderPass() = default;
};
} // namespace tg
