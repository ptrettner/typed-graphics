#pragma once

#include <typed-geometry/tg-lean.hh>

namespace tg
{
	// TODO: Cube and CubeArray
template <int D, class DataT>
class ImageView
{
    TG_REFERENCE_TYPE(ImageView);

public:
    using data_t = DataT;

    ImageView() = default;
};
} // namespace tg
