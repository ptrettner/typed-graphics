#pragma once

#include <typed-graphics/tg-lean.hh>

namespace tg
{
template <class FragmentT>
class FragmentShader
{
    TG_REFERENCE_TYPE(FragmentShader);

public:
    using fragment_t = FragmentT;

    FragmentShader() = default;
};
} // namespace tg