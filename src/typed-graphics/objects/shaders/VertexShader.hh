#pragma once

#include <typed-graphics/tg-lean.hh>

namespace tg
{
template <class VertexT>
class VertexShader
{
    TG_REFERENCE_TYPE(VertexShader);

public:
    using vertex_t = VertexT;

    VertexShader() = default;
};
} // namespace tg