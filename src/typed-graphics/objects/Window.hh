#pragma once

#include <typed-graphics/tg-lean.hh>

#include "Image.hh"
#include "ImageView.hh"

namespace tg
{
class Window
{
    TG_REFERENCE_TYPE(Window);

public:
    Window() = default;

public:
    struct Frame
    {
        TG_RAII_TYPE(Frame);

        Frame() {}
        ~Frame()
        {
            // TODO: actual present
        }

        isize2 size() const
        {
            return {}; // TODO
        }

        template <class FragmentT>
        void present(SharedImage2D<FragmentT> const& img)
        {
            present(img->view());
        }
        template <class FragmentT>
        void present(SharedImageView2D<FragmentT> const& img)
        {
            (void)img;
            // TODO
        }

        // TODO: RAII

        operator bool() const
        {
            // TODO
            return true;
        }
    };

    Frame startFrame() { return {}; }
};
} // namespace tg
