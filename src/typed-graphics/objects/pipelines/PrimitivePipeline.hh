#pragma once

#include <typed-graphics/tg-lean.hh>

#include <typed-graphics/data/vertex_format.hh>

namespace tg
{
/**
 * Graphics pipeline using primitive shaders (vertex, tessellation, geometry, fragment)
 *
 * A primitive pipeline consists of the following state:
 *	* All used shaders
 *	* Vertex format (attributes, topology, patch size)
 *	* Viewport information
 *	* Rasterization info (culling, polygonMode)
 *	* Blending
 *	* Depth/Stencil testing
 *	* DescriptorSet layouts
 *	* PushConstant ranges
 *
 * The pipeline is specific to a SubPass
 *
 * Some state can be configured to be set dynamically:
 *	* Viewport, Scissor, Line Width, Stencils
 *	* Depth Bias, Depth Bounds
 *	* Blend Constants
 *
 * TODO:
 *	* for now, viewport and scissor is always dynamic
 */
template <class VertexT, class FragmentT, class ConstantT, class... ResourceSets>
class PrimitivePipeline
{
    TG_REFERENCE_TYPE(PrimitivePipeline);

public:
    using vertex_t = VertexT;
    using fragment_t = FragmentT;
    using constant_t = ConstantT;

    PrimitivePipeline() = default;
};
} // namespace tg