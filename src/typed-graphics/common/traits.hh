#pragma once

#include <typed-geometry/tg-lean.hh>

#include <typed-graphics/data/type.hh>

namespace tg
{
template <class T>
struct type_of_t
{
};
template <class T>
constexpr type type_of = type_of_t<T>::value;

#define TG_DEFINE_TYPE(T)                      \
    template <>                                \
    struct type_of_t<T>                        \
    {                                          \
        static constexpr type value = type::T; \
    } // force ;

TG_DEFINE_TYPE(i32);
TG_DEFINE_TYPE(f32);
TG_DEFINE_TYPE(u32);

TG_DEFINE_TYPE(f8vec1);
TG_DEFINE_TYPE(f8vec2);
TG_DEFINE_TYPE(f8vec3);
TG_DEFINE_TYPE(f8vec4);

TG_DEFINE_TYPE(f16vec1);
TG_DEFINE_TYPE(f16vec2);
TG_DEFINE_TYPE(f16vec3);
TG_DEFINE_TYPE(f16vec4);

TG_DEFINE_TYPE(f32vec1);
TG_DEFINE_TYPE(f32vec2);
TG_DEFINE_TYPE(f32vec3);
TG_DEFINE_TYPE(f32vec4);

#undef TG_DEFINE_TYPE

} // namespace tg
