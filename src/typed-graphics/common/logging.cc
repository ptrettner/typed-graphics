#include "logging.hh"

#include <iostream>

static tg::logging::log_function sLogger;

void tg::logging::set_logger(log_function logf) { sLogger = std::move(logf); }

void tg::logging::log(severity severity, std::string const& message)
{
    if (sLogger)
        sLogger(severity, message);
    else
    {
        auto& oss = severity <= severity::info ? std::cout : std::cerr;
        static char const* types[] = {
            "diagnostic", //
            "info",       //
            "WARNING",    //
            "ERROR",      //
        };
        oss << "[TG][" << types[int(severity)] << "] " << message << std::endl;
    }
}
