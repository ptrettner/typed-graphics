#pragma once

#if TG_PROFILE_AION

#include <aion/Tracer.hh>
#define TG_TRACE(...) TRACE(__VA_ARGS__)

#else

#define TG_TRACE(...)

#endif
