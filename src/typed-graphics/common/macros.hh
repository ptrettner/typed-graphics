#pragma once

// disable inlining
#ifdef _MSC_VER
#define TG_NO_INLINE __declspec(noinline)
#else
#define TG_NO_INLINE __attribute__((noinline))
#endif

// [[nodiscard]] macro
#define TG_NO_DISCARD [[nodiscard]]

// makes SharedXyz types available
#define TG_SHARED(klass, type) \
    klass type;                \
    using Shared##type = std::shared_ptr<type> // force ;
#define TG_SHARED_T1(klass, type, T1) \
    template <class T1>               \
    klass type;                       \
    template <class T1>               \
    using Shared##type = std::shared_ptr<type<T1>> // force ;
#define TG_SHARED_T2(klass, type, T1, T2) \
    template <class T1, class T2>         \
    klass type;                           \
    template <class T1, class T2>         \
    using Shared##type = std::shared_ptr<type<T1, T2>> // force ;
#define TG_SHARED_T3(klass, type, T1, T2, T3) \
    template <class T1, class T2, class T3>   \
    klass type;                               \
    template <class T1, class T2, class T3>   \
    using Shared##type = std::shared_ptr<type<T1, T2, T3>> // force ;
#define TG_SHARED_T3D1(klass, type, T1, T2, T3, D3) \
    template <class T1, class T2, class T3 = D3>    \
    klass type;                                     \
    template <class T1, class T2, class T3 = D3>    \
    using Shared##type = std::shared_ptr<type<T1, T2, T3>> // force ;

// delete certain ctors/operators for our RAII usage
#define TG_RAII_TYPE(T)         \
    T(T&&) = default;           \
    T(T const&) = delete;       \
    T& operator=(T&&) = delete; \
    T& operator=(T const&) = delete // force ;

// unified interface for builders
// #define TG_BUILD(type)                                       \
//     type##Builder(type##Builder const&) = delete;            \
//     type##Builder& operator=(type##Builder const&) = delete; \
//     operator Shared##type() const { return build(); }        \
//     Shared##type build() const // expect { }

// non-copyable non-movable
#define TG_REFERENCE_TYPE(type)            \
    type(type const&) = delete;            \
    type(type&&) = delete;                 \
    type& operator=(type const&) = delete; \
    type& operator=(type&&) = delete // force ;

// literal concat with protection for macro invoc
#define TG_CONCAT(A, B) TG_IMPL_CONCAT1(A, B)
#define TG_IMPL_CONCAT1(A, B) TG_IMPL_CONCAT2(A, B)
#define TG_IMPL_CONCAT2(A, B) A##B

#define TG_EXPAND(x) x
#define TG_EXPAND_VA(...) __VA_ARGS__

// inspired by https://groups.google.com/forum/#!topic/comp.std.c/d-6Mj5Lko_s
// and https://stackoverflow.com/questions/1872220/is-it-possible-to-iterate-over-arguments-in-variadic-macros
#define TG_IMPL_FOREACH_1(MACRO, ARG, ...) MACRO(ARG)
#define TG_IMPL_FOREACH_2(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_1(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_3(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_2(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_4(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_3(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_5(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_4(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_6(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_5(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_7(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_6(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_8(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_7(MACRO, __VA_ARGS__)
#define TG_IMPL_FOREACH_9(MACRO, ARG, ...) \
    MACRO(ARG)                             \
    TG_IMPL_FOREACH_8(MACRO, __VA_ARGS__)

// TODO: fixme for MSVC
#define TG_IMPL_FOREACH_NTH_ARG(...) TG_EXPAND(TG_IMPL_FOREACH_ARG1(__VA_ARGS__, 8, 7, 6, 5, 4, 3, 2, 1, 0))
#define TG_IMPL_FOREACH_ARG1(...) TG_IMPL_FOREACH_ARG2(__VA_ARGS__) TG_IMPL_FOREACH_ARG3(__VA_ARGS__)
#define TG_IMPL_FOREACH_ARG3(...) "--" TG_IMPL_FOREACH_ARG2(TG_EXPAND_VA(__VA_ARGS__)) "--" #__VA_ARGS__
#define TG_IMPL_FOREACH_ARG2(_1, _2, _3, _4, _5, _6, _7, _8, N, ...) N
#define TG_IMPL_FOREACH_REVERSE_SEQ() 8, 7, 6, 5, 4, 3, 2, 1, 0

#define TG_IMPL_FOREACH(N, MACRO, ARG, ...) TG_EXPAND(TG_CONCAT(TG_IMPL_FOREACH_, N)(MACRO, ARG, __VA_ARGS__))
#define TG_FOREACH(MACRO, ARG, ...) TG_EXPAND(TG_IMPL_FOREACH(TG_EXPAND(TG_IMPL_FOREACH_NTH_ARG(ARG, __VA_ARGS__)), MACRO, ARG, __VA_ARGS__))
