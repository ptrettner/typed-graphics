#pragma once

#include <exception>

namespace tg
{
/// thrown when a backend cannot initialize itself
struct failed_to_initialize_exception : std::exception
{
};

/// thrown if an assertion failed
struct assert_exception : std::exception
{
};
} // namespace tg
