#pragma once

#include <functional>
#include <sstream>
#include <string>

#include "macros.hh"

/**
 * Logging helper
 *
 * Usage:
 *		tg::info() << "this is an info";
 *		tg::warning() << "you have " << myInt << " invalid thingies";
 *		tg::error() << "this shouldn't have happened";
 */

namespace tg
{
namespace logging
{
enum class severity
{
    diagnostic,
    info,
    warning,
    error
};

using log_function = std::function<void(severity, std::string const&)>;

/// sets a custom log callback
/// nullptr resets to default logging
void set_logger(log_function logf);

void log(severity severity, std::string const& message);

struct raii_logger
{
    TG_RAII_TYPE(raii_logger);

    raii_logger(severity severity) : severity(severity) {}
    ~raii_logger() { log(severity, ss.str()); }

    severity severity;
    std::stringstream ss;
};

template <class T>
raii_logger&& operator<<(raii_logger&& logger, T const& value)
{
    logger.ss << value;
    return std::move(logger);
}
} // namespace logging

inline logging::raii_logger diagnostic() { return {logging::severity::diagnostic}; }
inline logging::raii_logger info() { return {logging::severity::info}; }
inline logging::raii_logger warning() { return {logging::severity::warning}; }
inline logging::raii_logger error() { return {logging::severity::error}; }

} // namespace tg