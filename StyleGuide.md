# Style Guide


## Formatting

* The `.clang-format` in this repository is mandatory


## Types

* Reference types in `UpperCamelCase` (and `class`)
* Value types in `lower_snake_case` (and `struct`)
* RAII types in `UpperCamelCase` (and `class`)
* Reference types `Name` have `SharedName` typedefs for `std::shared_ptr<Name>`
* Template type parameters in `UpperCamelCaseT` with an optional `using lower_snake_case_t = UpperCamelCaseT`
* Interface types in `UpperCamelCase` (and `class`)


## Methods

* Methods of `UpperCamelCase` types in `lowerCamelCase()`
* Methods of `lower_snake_case` types in `lower_snake_case()`
* Free methods in `lower_snake_case()`
* For value types, prefer free methods


## Members

* Value types get public members in `lower_snake_case`
* Reference types get private members in `_lowerCamelCase` (prefix `_`)


## Parameters

* East-side const, especially `int const&` instead of `const int&`
* `SharedType`s should be passed as `const&`


## Casts

* For built-in primitives, C casts are OK
* For complex type, `static_cast`, `dynamic_cast`, etc. are mandatory


## Namespaces

* Namespaces are `lower_snake_case`
* Namespaces should be short if used often
* `detail` namespace is used for internal code and should not be used externally


## Exceptions

* Exceptions are value types 
* Exceptions are used sparingly and never in normal execution context
* They should only be used for non-locally-recoverable problems such as failing to initialize vulkan
