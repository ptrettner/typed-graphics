# typed-graphics

Graphics library with strongly typed interfaces.


## Usage / Example


```
#include <tg/typed-graphics.hh>

// TODO
```


## Dependencies

`typed-graphics` expects dependencies as CMake targets.

### Quickstart

Vulkan example (with some recommended dependencies):

```
mkdir extern
cd extern
git submodule add https://github.com/zeux/volk.git
git submodule add https://www.graphics.rwth-aachen.de:9000/ptrettner/typed-geometry.git
git submodule add https://www.graphics.rwth-aachen.de:9000/ptrettner/typed-graphics.git
git submodule add https://www.graphics.rwth-aachen.de:9000/ptrettner/polymesh.git
git submodule add https://www.graphics.rwth-aachen.de:9000/ptrettner/imgui-lean.git
git submodule add https://github.com/glfw/glfw.git
```

Optionals:

```
git submodule add https://www.graphics.rwth-aachen.de:9000/ptrettner/ctracer.git
git submodule add https://github.com/g-truc/glm.git
git submodule add https://github.com/google/googletest.git
git submodule add https://github.com/emil-e/rapidcheck.git
```

### Required

* `typed-geometry` math library
* `glfw` window library (required for now)

### Backend Dependencies

(Don't forget to install the vulkan SDK)

* `volk` Vulkan loader
* `lava` Vulkan library
* `glow` OpenGL library

### Optional

* `glm` math library
* `polymesh` mesh library
* `imgui` UI library
* `aion` profiling library
* `glfw` window library


## CMake Usage

```
cmake_minimum_required(VERSION 3.8)
project(MyProject)


# ===============================================
# Global settings

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set(BUILD_SHARED_LIBS ON) # required by some dependencies


# ===============================================
# Dependencies

# math library
add_subdirectory(extern/typed-geometry)

# graphics library
add_subdirectory(extern/typed-graphics)

# mesh library
add_subdirectory(extern/polymesh)


# ===============================================
# Create target

file(GLOB_RECURSE SOURCES
    "src/*.cc"
    "src/*.hh"
)

source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${SOURCES})

add_executable(MyProject ${SOURCES})

target_include_directories(MyProject PUBLIC "src")

target_link_libraries(MyProject PUBLIC
    typed-geometry
    typed-graphics
    polymesh
)


# ===============================================
# Compile flags

if (MSVC)
    target_compile_options(MyProject PUBLIC 
        /MP
    )
else()
    target_compile_options(MyProject PUBLIC 
        -Wall
        -Wno-unused-variable
    )
endif()

```

## Typing Concept

* `<tg/objects/...>` contains strongly typed graphics objects (backend-independent)
* `<tg/untyped/...>` contains weakly typed graphics objects (backend-independent)
* `<tg/backend/...>` contains backend-specific implementations

The strongly typed objects should be seen as almost immutable.
Mutability is kept to a minimum and is only used when immutability significantly hinders productivity, e.g. resizing targets when the window size is changed.
When an object is mutated, a generation counter is increased and all dependent objects are lazily regenerated.

Strong typing is achieved with many templates which makes the API almost header-only.
This also makes many aspects of graphics objects compile-time static.
In order to transparently change backends and support runtime typing, the `untyped` layer exists.
Backends purely operate on the untyped layer.

Untyped objects are basically create infos with a custom payload from backends.
Typed objects are thin layers on top of untyped ones.
